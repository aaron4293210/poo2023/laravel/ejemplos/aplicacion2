<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});

Route::get('/mensaje', function () {
    $mensaje = "Esto es un mensaje";

    return view('mensaje', ['mensaje' => $mensaje]);
});

Route::get('/listado', function () {
    $nombres = ["Pepe", "Pepa", "Pepito", "Pepete"];

    return view('listado', ['nombres' => $nombres]);
});

Route::get('/imagen', function () {
    return view('imagen');
});

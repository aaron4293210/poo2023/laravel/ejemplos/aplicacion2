<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Imagen</title>
    @vite('resources/scss/app.scss')
</head>
<body>
    @include('_menu') 

    <div class="container">
        <div class="row my-4">
            <div class="card col-md-9 mb-4 mx-auto text-center"> 
                <div class="card-header bg-transparent">
                    <img class="card-img-top img-fluid" alt="Koala" src="/imgs/koala.jpg">
                </div>
                    
                <div class="card-body">
                    <h4 class="card-title font-italic text-primary">FOTO</h4>
                    <p class="card-text lead text-muted">Koala</p>
                    
                </div>
            </div>
        </div>
    </div>

    @vite('resources/js/app.js')
</body>
</html>
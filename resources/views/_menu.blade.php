<nav class="navbar navbar-expand-lg bg-body-tertiary"  data-bs-theme="dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="http://aplicacion2.test/">Aplicacion 2</a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="http://aplicacion2.test/">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="http://aplicacion2.test/mensaje">Mensaje</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="http://aplicacion2.test/listado">Listado</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="http://aplicacion2.test/imagen">Imagen</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
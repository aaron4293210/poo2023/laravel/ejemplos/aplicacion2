<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Listado</title>
    @vite('resources/scss/app.scss')
</head>
<body>
    @include('_menu')

    <div class="container my-4">
        <div class="row">
            @foreach ($nombres as $nombre)
                <div class="card-group col-md-4 mb-4 text-center">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title font-italic text-primary">NOMBRES</h4>
                        </div>
    
                        <div class="card-body text-center">
                            <p class="card-text lead text-muted">{{ $nombre }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    
    @vite('resources/js/app.js')
</body>
</html>
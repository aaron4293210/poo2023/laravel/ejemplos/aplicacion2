<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mensaje</title>
    @vite('resources/scss/app.scss')
</head>
<body>
    @include('_menu')

    <div class="container my-4">
        <div class="row">
            <div class="alert alert-success text-center">{{ $mensaje }}</div>
        </div>
    </div>

    @vite('resources/js/app.js')
</body>
</html>